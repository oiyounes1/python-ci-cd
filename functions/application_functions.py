from dateutil import relativedelta as rd

def cpercent(amount, total, nb_decimal):
    '''
        Compute percent with specified number of decimal
    '''
    return round((amount/total*100), nb_decimal)

def gdelta_age(dtstart, dtend):
    '''
        Compute age and return year and months
    '''
    delta = rd.relativedelta(dtstart, dtend )
    return delta.years, delta.months

def gdelta(dtstart, dtend):
    '''
        Compute Global delta with dateutil.relativedelta
    '''
    delta = rd.relativedelta(dtend, dtstart )
    return delta

def nbMonths(dtstart, dtend):
    '''
        Compute delta in months between 2 dates
    '''
    delta = gdelta(dtend,dtstart)
    return ( delta.years * 12 ) + delta.months
