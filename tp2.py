
# # Boucle de lecture ligne à ligne
# with open('promotion_B3_B.csv', 'r') as file :
#     # On saute la premier ligne
#     next(file)
#     for line in file:
#         fields = line.split(";")
#         print(f"{fields[1]} - {fields[0]} ")

# # import time
# # time.sleep(900)  # 15 minutes en secondes


# import csv

# def analyser_promotion(fichier_csv):
#     total_eleves = 0
#     total_filles = 0
#     total_garcons = 0

#     with open(fichier_csv, 'r', encoding='utf-8') as file:
#         lecteur_csv = csv.reader(file, delimiter=';')

#         next(lecteur_csv)
#         for ligne in lecteur_csv:
#             total_eleves += 1
#             genre = ligne[2].strip().lower()

#             if genre == 'f':
#                 total_filles += 1
#             elif genre == 'h':
#                 total_garcons += 1
#     pourcentage_filles = round((total_filles / total_eleves) * 100, 2)
#     pourcentage_garcons = round((total_garcons / total_eleves) * 100, 2)
#     pourcentage_autres = round(((total_eleves - total_filles - total_garcons) / total_eleves) * 100, 2)

#     print(f"Nombre total d'élèves : {total_eleves}")
#     print(f"Nombre total de filles : {total_filles} - {pourcentage_filles}%")
#     print(f"Nombre total de garçons : {total_garcons} - {pourcentage_garcons}%")
#     print(f"Autres : {total_eleves - total_filles - total_garcons} - {pourcentage_autres}%")

# analyser_promotion('promotion_B3_B.csv')
import argparse
from datetime import datetime

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--path", type=str)

args = parser.parse_args()
file = args.path

if file is None:
    file = "promotion_B3_B.csv"

# Boucle de lecture ligne à ligne
with open(file, 'r') as file:
    nbrf = nbrh = nbro = 0
    # On saute la premier ligne
    next(file)
    for line in file:
        fields = line.split(";")
        gender = fields[2]
        birthdate_str = fields[3]  # Assuming the birthdate is in the fourth column

        if gender.lower() == 'h':
            nbrh += 1
        elif gender.lower() == 'f':
            nbrf += 1
        else:
            nbro += 1

        # Calculate age
        try:
            birthdate = datetime.strptime(birthdate_str, "%Y-%m-%d")  # Adjust the format if needed
            today = datetime.today()
            age = today.year - birthdate.year - ((today.month, today.day) < (birthdate.month, birthdate.day))
            months = (today.year - birthdate.year) * 12 + today.month - birthdate.month

            # Print student information with age
            print(f"{fields[1]} - {fields[0]} - gender: {gender} - Age: {age} ans et {months % 12} mois")
        except ValueError:
            print(f"Error parsing birthdate for {fields[1]} - {fields[0]}")

nbr_tot = nbrf + nbrh + nbro
sep = "=" * 80
print(sep)
print(f"Nombre total d'élèves : {nbr_tot}")
print(f"Nombre total de filles : {nbrf} - {round((nbrf/nbr_tot)*100, 2)} %")
print(f"Nombre total de garçons : {nbrh} - {round((nbrh/nbr_tot)*100, 2)} %")
print(f"Autres : {nbro} - {round((nbro/nbr_tot)*100, 2)} %")
print(sep)
print("fin du programme...")
    