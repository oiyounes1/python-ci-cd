import pytest 
import datetime as dt
from datetime import date as date
from dateutil import relativedelta as rd
import sys

# Add the path to the folder containing application_functions.py to sys.path
sys.path.insert(0, '/Users/oiyou/Documents/etudes23.4/tp python/2nd/functions/')

# Import the module
import application_functions as af

def test_percent_is_rounded():
    '''
        Test Unitaire de non-régression 
        Test permettant de contrôler la stabilité de calcul du pourcentage
    '''
    m1 = af.cpercent(2,36, 2)
    m2 = af.cpercent(33, 36, 2)
    m3 = af.cpercent(1, 36, 2)
    assert round(m1+m2+m3) == 100.00 , "La fonction cpercent est déstabilisée ..."
    print(f">>>>>>>> rouded >>>> {m1} + {m2} + {m2} = {round(m1+m2+m3)}")

def test_compute_date_limits():
    '''
        Test Unitaire de non-régression 
        Test permettant de contrôler la stabilité de la fonction gdelta_age
    '''
    today=date.today()
    dtbornj=today+rd.relativedelta(years=-18)
    dtbornj_minus_1=dtbornj+rd.relativedelta(days=-1)
    dtbornj_plus_1=dtbornj+rd.relativedelta(days=+1)
    ans, mois = af.gdelta_age(today,dtbornj )
    assert ( ans == 18 and mois == 0) , "La fonction gdelta_age doit renvoyer 18 ans et 0 mois"
    ans, mois = af.gdelta_age(today,dtbornj_minus_1 )
    assert ( ans == 18 and mois == 0) , "La fonction gdelta_age doit renvoyer 18 ans et 0 mois"
    ans, mois = af.gdelta_age(today,dtbornj_plus_1 )
    assert ( ans == 17 and mois == 11 ) , "La fonction gdelta_age doit renvoyer 17 ans et 11 mois"
    print(f">>>>>>>>>>>> date J {dtbornj}, dateJ+1:{dtbornj_plus_1}, dateJ-1={dtbornj_minus_1}")

def test_assert_age_inmonths():
    today=date(2023,11,17)
    dtborn=date(2003,6,30)
    nbMois = af.nbMonths(today, dtborn)
    assert nbMois == 244, "La fonction nbMonths est altérée ! "
    print(f">>>>>>>>>>>>Date1 :{today} , Date2 :{dtborn} , Nbmois :{nbMois}")
