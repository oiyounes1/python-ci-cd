def check_value(label):
    # Documentation Interne
    '''
        Fonction permettant la saisie et le contrôle des valeurs de suffrage
        1/ La saisie doit être numérique 
        2/ La saisie doit être > 0 et <= 100
    '''
    while True:
        rep = input(f"Entrez le score du candidat {label} : ? ")
        if rep == '':
            return rep
        try:
            rep = int(rep)
        except ValueError:
            print(f"Entrez une valeur numérique : {rep} ")
            continue
        # Ajout des tests fonctionnels
        if rep < 0 or rep > 100:
            print(f"La valeur doit être comprise entre 0 et 100 : {rep} ")
            continue
        # Ici tous les sont effectués et la valeur est correcte
        return rep


while True:
    A = check_value("A")
    if A == '':
        break

    B = check_value("B")
    C = check_value("C")
    D = check_value("D")

    C1 = A > 50

    C2 = B > 50 or C > 50 or D > 50
    C3 = A >= B and A >= C and A >= D
    C4 = A >= 12.5

    if C1:
        print("Elu au prmier tour")
    elif C2 or not C4:
        print("Battu éliminé, sorti ")
    elif C3:
        print("Ballotage favorable")
    else:
        print("Ballotage défavorable")

print("Fin du programme...")