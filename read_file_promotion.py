'''
    Programme de lecture et d'analyse d'un fichier structuré (CSV)
'''
# Boucle de lecture ligne à ligne
file_path = r'C:/Users/oiyou/Documents/etudes23.4/tp python/2nd/promotion_B3_B.csv'
with open(file_path, 'r') as file:
    # On saute la premier ligne
    next(file)
    for line in file:
        fields = line.split(";")
        if len(fields) >= 2:
            print(f"{fields[1]} - {fields[0]}")

print("fin du programme...")
    
