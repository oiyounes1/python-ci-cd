# file1 = open("fichier1.txt")

# # Boucle de lecture ligne à ligne
# idx = 1
# for line in file1:
#     print(f"{idx} - {line}")
#     idx += 1

# file1.close()
import os

file_path = "fichier1.txt"

# Check if the file exists
if os.path.exists(file_path):
    with open(file_path, "r") as file1:
        idx = 1
        for line in file1:
            print(f"{idx} - {line}")
            idx += 1
else:
    print(f"File not found: {file_path}")
